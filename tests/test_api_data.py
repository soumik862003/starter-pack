import os
import json
import math
from io import BytesIO

def create_data(client):
    """
    Create input data
    """
    input_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        '..', 'sample', 'input.csv')
    with open(input_file, 'rb') as f:
        content = f.read()
    data = {
        'someAttribute': '85'
    }
    res = client.post('/data', data=data)
    assert res.status_code == 200
    assert res.json['someAttribute'] == 85
    return res
