# pylint: disable=no-member,too-many-instance-attributes
from some_api.db import db

# Define the input data attributes here
class InputData(db.Document):
    someAttribute = db.StringField(required=True)
    
