
from flask import Flask

from some_api.db import db
from some_api.apis import api
from some_api import settings


def create_app():
    """
    Create flask app
    """
    app = Flask(__name__)

    app.config['MONGODB_SETTINGS'] = {
        'db': settings.MONGODB_SETTINGS_DB,
        'username': settings.MONGODB_SETTINGS_USERNAME,
        'password': settings.MONGODB_SETTINGS_PASSWORD,
        'host': settings.MONGODB_SETTINGS_HOST,
        'port': settings.MONGODB_SETTINGS_PORT
    }
    db.init_app(app)

    api.init_app(app)
    return app
