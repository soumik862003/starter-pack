import os

ROOT = os.path.dirname(os.path.abspath(__file__))

# Mongodb settings
MONGODB_SETTINGS_URI = os.getenv('MONGODB_URI', 'mongodb://root:topcoder@localhost:27017/admin')
PORT = os.getenv('PORT', '5000')

# default PerPage parameter for GET /data
PER_PAGE = 20
