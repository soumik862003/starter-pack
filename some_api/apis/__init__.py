from flask_restplus import Api

from .data import api as data_api

api = Api(
    title='Quartz Energy - Some Services API',
    version='1.0',
)

api.add_namespace(data_api)