import json
import flask
from flask_restplus import Namespace, Resource, fields, abort

from some_api.db.models import InputData
from some_api.service.input_data_service import create_input_data

api = Namespace('data', description='data endpoints')

# Use RestPlus to define Swagger models
input_data_model = api.model('Data', {
    'id': fields.String(readOnly=True, description='The input data identifier', attribute='pk'),
    'someProperty': fields.String(
        required=True, description='The input data some property', attribute='someAttribute'),
})

# define swagger models for response too


@api.route('')
@api.response(200, 'ok')
@api.response(400, 'Bad request')
@api.response(500, 'Internal server error')
class DataUpload(Resource):
    @api.doc('Import calculation data.')
    @api.marshal_with(input_data_model)
    def post(self):
        '''
        Import calculation data.
        '''
        args = upload_parser.parse_args()
        
        # additional validation here

        data = create_input_data(args)
        return data

# Define all data management endpoints here