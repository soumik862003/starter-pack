import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

package_name="some-service-api"
script="scripts/some-api"

setuptools.setup(
    name=package_name,
    version="0.0.1",
    author="topcoder",
    author_email="support@topcoder.com",
    description=package_name,
    long_description=long_description,
    long_description_content_type="text/markdown",
    scripts=[script,],
    packages=setuptools.find_packages(exclude=('tests', 'docs')),
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
)
